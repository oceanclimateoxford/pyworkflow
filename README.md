# README #

### What is this repository for? ###

This repository contains code that shows some of the features of python and IPython notebook.

### What's with all the sequentially named files? ###

*  **workflow.ipynb** shows some of the basic features and code being developed in an IPython notebook.
    * The notebook can be viewed [here](http://nbviewer.ipython.org/urls/bitbucket.org/oceanclimateoxford/pyworkflow/raw/4a30076dc37f3ef18bfb1215fc823a2bbf3af3a7/workflow.ipynb) 
*  **workflow.py** is the notebook downloaded as python code.
* **workflow0.py** is the code after it's been cleaned up and had the extra lines from the notebook removed.
* **workflow1.py** is the code turned into a callable python function.
* **workflow2.py** has some extra code to provide a help interface from the terminal and prints time in UTC as the plot title.
* **workflow2.ipynb** imports workflow2.py and calls the function from the notebook.
    * The notebook can be viewed [here](http://nbviewer.ipython.org/urls/bitbucket.org/oceanclimateoxford/pyworkflow/raw/4a30076dc37f3ef18bfb1215fc823a2bbf3af3a7/workflow2.ipynb)




The code was devloped by [Jonas](http://www.gfy.ku.dk/~bluthgen/) and [Ed](http://doddridge.me).