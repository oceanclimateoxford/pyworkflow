
# coding: utf-8

## Workflow with python

# This is an IPython Notebook. 
# 
# It lets you combine **text**, **code**, **graphs**, **animations**, and even $\LaTeX$ and store and share them in **one self-contained file**.

# In[1]:

from IPython.display import Image
Image('python.png')


# Start by importing the modules you need.

# In[2]:

import netCDF4 # for reading netCDF files
import matplotlib.pyplot as plt # for plotting
get_ipython().magic(u'matplotlib inline')
plt.rcParams['figure.figsize'] = (12, 12) # set default figure size to 12x12 inches


# We use some [Hadley Centre SST data](http://www.metoffice.gov.uk/hadobs/hadisst/data/download.html) for demonstration

# In[3]:

fname = 'HadISST1_SST_update.nc'
#!wget -O $fname http://www.metoffice.gov.uk/hadobs/hadisst/data/HadISST1_SST_update.nc.gz 

ds = netCDF4.Dataset(fname, mode='r')


# In[4]:

print '\n'.join(ds.variables)


# In[5]:

sst = ds.variables['sst']


# In[6]:

print sst


# In[7]:

timelev = -1
sst_time_slice = sst[timelev,:,:]


# In[8]:

im = plt.imshow(sst_time_slice, vmin=-5, vmax=35)
cb = plt.colorbar(im, shrink=0.4)


# But, let's say we don't like the default `jet` colour map... Let's change it.
# 
# Go to the reference page of [colour maps already included in matplotlib]( http://matplotlib.org/examples/color/colormaps_reference.html). All the great [colorbrewer](http://colorbrewer2.org/) ones are, too.

# In[9]:

im = plt.imshow(sst_time_slice, cmap='RdBu', vmin=-5, vmax=35)
cb = plt.colorbar(im, label='degrees C', shrink=0.4)

