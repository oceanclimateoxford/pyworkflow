"""
Typical workflow:

    1. Start with writing a script (all hard-coded)
    2. Refine until it works for one file/parameter
    3. Refactor into function(s)
    4. Make command-line interface

"""
import netCDF4 # for reading netCDF files
import matplotlib.pyplot as plt # for plotting

def plot_sst(
    fname = 'HadISST1_SST_update.nc',
    timelev = -1):

    ds = netCDF4.Dataset(fname, mode='r')

    sst = ds.variables['sst']

    sst_time_slice = sst[timelev,:,:]

    im = plt.imshow(sst_time_slice, cmap='RdBu_r', vmin=-5, vmax=35)
    cb = plt.colorbar(im, label='degrees C', shrink=0.4)

    plt.show()

plot_sst()
