"""Code refactored into a function"""
import netCDF4 # for reading netCDF files
import matplotlib.pyplot as plt # for plotting


def plot_sst(
        fname = 'HadISST1_SST_update.nc',
        timelev = -1):
    """Ed and Jonas' awesome SST plotter"""

    ds = netCDF4.Dataset(fname, mode='r')

    sst = ds.variables['sst']

    sst_time_slice = sst[timelev,:,:]

    im = plt.imshow(sst_time_slice, cmap='RdBu_r', vmin=-5, vmax=35)
    cb = plt.colorbar(im, label='degrees C', shrink=0.4)

    plt.show()


if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(description='Plot SST data')
    parser.add_arguments('--timelev', type=int, default=-1, help='time level')
    
    args = parser.parse_args()

    plot_sst(timelev=args.timelev)
